package br.com.tcs.servlet;

import br.com.tcs.dao.CustomerDao;
import br.com.tcs.model.Customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
@WebServlet(urlPatterns = "/")
public class CustomerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        CustomerDao customerDao = new CustomerDao();
        List<Customer> customerList = new ArrayList<Customer>();

        if (req.getParameter("customer_id") != null) {
            int customer_id = Integer.parseInt(req.getParameter("customer_id"));
            Customer withId = customerDao.getWithId(customer_id);
            customerList.add(withId);
        } else {
            customerList = customerDao.getAllCustomers();
        }

        req.setAttribute("customers", customerList);
        req.getRequestDispatcher("/WEB-INF/jsp/customer.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String first_name = req.getParameter("first_name");
        String last_name = req.getParameter("last_name");

        CustomerDao customerDao = new CustomerDao();
        customerDao.addNewCustomer(first_name, last_name);

        resp.sendRedirect(getServletContext().getContextPath()+"/");

    }
}
